# Ansible install kubernetes



## Getting started

To get started on this project I first created the following file structure: 

```bash
kube-deployment/
├── playbooks/
│   ├── install-rke2.yml
│   ├── configure-rke2.yml
│   ├── deploy-cluster.yml
│   └── ...
├── inventory/
│   └── hosts.ini
├── files/
│   ├── rke2/
│   │   ├── rke2.linux-amd64
│   │   ├── rke2-install.sh
│   │   └── ...
│   ├── container-images/
│   │   ├── rke2-images.tar
│   │   ├── ...
│   └── ...
├── roles/
│   ├── common/
│   │   ├── tasks/
│   │   │   ├── install-dependencies.yml
│   │   │   └── ...
│   │   └── ...
│   ├── rke2/
│   │   ├── tasks/
│   │   │   ├── install-rke2.yml
│   │   │   ├── configure-rke2.yml
│   │   │   ├── ...
│   │   └── ...
│   └── ...
├── site.yml
├── ansible.cfg
└── ...
```
In this structure:

    - playbooks directory contains Ansible playbooks.
    - inventory directory includes Ansible inventory files.
    - files directory holds files like the RKE2 binary (rke2.linux-amd64), installation scripts, configuration files, and container images.
    - roles directory can have Ansible roles to organize your playbook logic.
    - ansible.cfg is the Ansible configuration file. You can specify variables, settings, or plugins here.

## Set up SSH between Master and Worker Nodes

First populate and create the hosts file 

```bash
[masters]
master ansible_host=192.168.4.71 ansible_user=root

[workers]
worker1 ansible_host=192.168.4.72 ansible_user=root
worker2 ansible_host=192.168.4.73 ansible_user=root
worker3 ansible_host=192.168.4.74 ansible_user=root

[all:vars]
ansible_python_interpreter=/usr/bin/python3
```
## Create a file in playbooks called ssh-setup.yaml
```bash
- name: SSH Setup Play
  hosts: masters # Defines which hosts or group this playbook applies to
  become: yes # Allows Ansible to run tasks with escalated privileges like sudo
  tasks:
    - name: Create the 'ansible' user on the master
      user:
        name: ansible
        append: yes # If the user already exists, it appends to the user's data rather than replacing it.
        state: present # Ensures the user exists
        createhome: yes #Creates the home directory for the user
        shell: /bin/bash # Specifies the user's shell
# This task creates the 'ansible' user on the master node, which will be sued for SSH authentication
    - name: Ensure .ssh directory exists on the master
      file: # Ansible module used for file operations
        path: /home/ansible/.ssh 
        state: directory # Ensures the directory exists
        owner: ansible # Sets ownership
        group: ansible
        mode: '0700' # Sets the directory permissions

    - name: Generate SSH Key Pair on the master
      ansible.builtin.openssh_keypair: # This ansible module generates an SSH key pair
        path: /home/ansible/.ssh/id_rsa
        type: rsa
        size: 2048
      register: ssh_keypair # registers the key for later use
      # This task generates an RSA SSH key pair on the master node. The private key ('id_rsa') is stored in the '.ssh' direcotry, and the public key ('id_rsa.pub') will be used for authentication.

    - name: Set proper permissions for the SSH key on the master
      file:
        path: /home/ansible/.ssh/id_rsa
        owner: ansible
        group: ansible
        mode: '0600'
        # This task ensures that the private key has the correct permissions to keep it secure

- name: Fetch the SSH public key from the master to the control machine
  hosts: masters
  tasks:
    - name: Fetch the SSH public key
      fetch: # This module is used for fetching files from remote machines and storing them locally in a file tree, organized by hostname.
        src: /home/ansible/.ssh/id_rsa.pub # Specifies the source file on the master, which is the public key
        dest: /tmp/ #specifies the destination on the control machine
        flat: yes # Flattens the directory

- name: Create the 'ansible' user on the workers
  hosts: workers
  become: yes
  tasks:
    - name: Create the 'ansible' user on the workers
      user:
        name: ansible
        append: yes
        state: present
        createhome: yes
        shell: /bin/bash
        # This creates the 'ansible' user on workers

    - name: Ensure .ssh directory exists on the workers
      file:
        path: /home/ansible/.ssh
        state: directory
        owner: ansible
        group: ansible
        mode: '0700'

    - name: Copy the SSH public key from the control machine to the worker nodes
      copy:
        src: /tmp/id_rsa.pub
        dest: /home/ansible/.ssh/authorized_keys
        owner: ansible
        group: ansible
        mode: '0600'
```
- Run the play with: 
```bash
ansible-playbook -i inventory/hosts.ini playbooks/ssh-setup.yml 
```
## Create a Play to get the RKE2 Dependencies

```bash
- name: Download and Extract RKE2 Package
  hosts: localhost
  tasks:
    - name: Create the target directory for RKE2
      file:
        path: /home/kev/kube-cluster/ansible-install-kubernetes/files/rke2
        state: directory

    - name: Download RKE2 package
      command: curl -o /home/kev/kube-cluster/ansible-install-kubernetes/files/rke2/rke2-package.tar.gz -OLs https://github.com/rancher/rke2/releases/download/v1.28.2%2Brke2r1/rke2-images.linux-amd64.tar.gz

    - name: Extract RKE2 package
      command: tar -zxvf /home/kev/kube-cluster/ansible-install-kubernetes/files/rke2/rke2-package.tar.gz -C /home/kev/kube-cluster/ansible-install-kubernetes/files/rke2
```
- Run with:
```bash
ansible-playbook -i inventory/hosts.ini playbooks/download_and_extract_rke2.yml.yml 
```
Populating Files:

    Place the rke2.linux-amd64 binary and RKE2 installation script (if required) in the files/rke2 directory.
    If you have specific container images for RKE2 components, save them as a Docker tarball (e.g., rke2-images.tar) and place them in the files/container-images directory.
    Store any required configuration files, like rke2.yaml, in the files directory.

Ansible Playbooks:

    Create playbooks for tasks like installing RKE2 (install-rke2.yml), configuring RKE2 (configure-rke2.yml), and deploying a Kubernetes cluster (deploy-cluster.yml).

Roles:

    Organize your playbook logic into Ansible roles. For example, create a common role to handle common tasks and an rke2 role to manage RKE2-related tasks.

Inventory:

    Configure your inventory in the inventory/hosts.ini file to specify the remote servers where you want to install RKE2.

Ansible Configuration:

    Customize the ansible.cfg file to define settings such as where to find your roles, playbooks, and inventory files.

Playbook Execution:

    When executing your playbooks, ensure that the required files (binaries, images, configuration) are referenced from the local files directory, and Ansible copies them to the remote servers during playbook execution.

Offload Files to Remote Servers:

    If you want to transfer files to remote servers before running tasks, you can use Ansible's copy or synchronize modules within your playbooks. This will ensure the required files are available on the servers.

Execute Playbooks:

    Run your playbooks with ansible-playbook to automate the installation and configuration of RKE2 on your remote servers.
